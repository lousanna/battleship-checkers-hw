//
//  battleship.h
//  hw5
//
//  Created by HuangXinyu on 4/12/16.
//

#include "game.h"
#include <array>
#include <regex>
#include <vector>

enum Ship {
    AIRCRAFT,
    BATTLESHIP,
    CRUISER,
    SUBMARINE,
    DESTROYER
};

typedef struct{
    Coord coordinate;
    char orientation;
} status;

class BattleshipGame:public Game{
public:
    BattleshipGame(std::ostream &output = std::cout,std::istream &input = std::cin):ship_status(),out(output),in(input){
        setBattleship(0);
        setBattleship(1);
	out<<"BEGIN\n";
    }
    virtual GameResult attack_square(Coord coordinate);
    friend std::ostream& operator<<(std::ostream & out, const BattleshipGame & bs);

protected:
    Board battleship[2] = {Board(10),Board(10)};

    //initialize the Board
    void setBattleship(int playerNum);

    //check whether the game has ended or not
    GameResult check_status(void);
    
    status prompt(int playerNum, int shipNum);
    void placeShip(int playerNum, int shipNum, status location);
    std::vector<int> removeShip(int playerNum, status ship);

    bool checkValid(int shipNum, int player, status location);
    bool checkUserInput(std::string place);
    bool check_sunk(int player_num,int shipNum);
    int check_miss(int player, Coord coordinate);

    std::pair<std::string,int> ships[5] = {std::make_pair("AIRCRAFT CARRIER",5), std::make_pair("BATTLESHIP",4), std::make_pair("CRUISER",3), std::make_pair("SUBMARINE",3),std::make_pair( "DESTROYER",2)};
    status ship_status[2][5];
    std::ostream & out;
    std::istream & in;
};
