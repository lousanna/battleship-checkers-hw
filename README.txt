DESIGN:
Since all three types of games have boards and have similar game conditions, game were inherited for all three games and board is a component of game. 
Mobile battleship was very similar to battleship so it inherited battleship.

REQUIRED LANGUAGE FEATURES:
Operator overload to print out what player turn it is and also victory message or draw message. When << is given either a "Board" or "Game", it has different outputs.
Inheritance of board and game to both battleship, mobile battleship, and checkers. 
Polymorphism in Battleship games.
battleship and mobile battleship both have a virtual function attack_square. The mobile battleship one overrides the one in battleship and call BattleshipGame::attack_square in its own function
We used several STL containers in our programs, such as array, vector and pair

COMPLETENESS:
There are no known issues in checkers. Double jumps, selecting wrong pieces or empty squares, not jumping when required, trying to make impossible jumps, trying to moveout of bounds, triple jumps, and even four jumps in a row havebeen tested for checkers.
No known issues with battlshipe or mobile battleship.


SPECIAL INSTRUCTION:
To make all the executables, type make, which makes the main executable and all the test executables.

To run the test for checkers, run "./play_checkers". If all tests are passed, it will output that game 1 through game 5 have passed, then all tests passed.

To run the test for battleship, run "./play_bs". 
It will read input from two input files, bs_in_1.txt and bs_in_2.txt
and output two files, bs_out_1.txt and bs_out_3.txt
which will be compared to two pre-prepared output files, bs_out_2.txt and bs_out_4.txt
Output "SUCCESS!" to the terminal if passes all tests


To run the test for mobile battleship, run "./play_mobile_bs"
It'll read input from bs_in_1.txt
and output bs_out_5.txt, which will be compared to bs_out_6.txt
Output "SUCCESS!" to terminal.
