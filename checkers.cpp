/*Final Project checkers.cpp, 600.120 Spring 2016
 *
 *
 *Peiyong(Steven) Zhang, 600.120, 4/27/15,Final Project, 4435781967, pzhang18, pzhang18@jhu.edu
 */

#include "checkers.h"
#include "game.h"
#include <string>
using namespace std;




bool CheckersGame::check_jump(){
	if(check_status()==2){
		return true;
	}else{
		return false;
	}
}



bool CheckersGame::additional_jumps(Coord piece, int current_player_piece){
	int x= piece.first;
	int y= piece.second;

	if(current_player_piece==2){
		if(top_left_jump(x,y)){
			continued_jump=make_pair(x,y);
			keep_jumping=true;
			return true;
		}else if(top_right_jump(x,y)){
			keep_jumping=true;
			continued_jump=make_pair(x,y);
			return true;
		}else{
			counter++;
			keep_jumping=false;
			return false;
		}
	}

	if(current_player_piece==3){
		if(bottom_left_jump(x,y)){
			continued_jump=make_pair(x,y);
			keep_jumping=true;
			return true;
		}else if(bottom_right_jump(x,y)){
			keep_jumping=true;
			continued_jump=make_pair(x,y);
			return true;
		}else{
			counter++;
			keep_jumping=false;
			return false;
		}
	}


	if(current_player_piece==4 || current_player_piece==5){
		if(top_left_jump(x,y)){
			continued_jump=make_pair(x,y);
			keep_jumping=true;
			return true;
		}else if(top_right_jump(x,y)){
			continued_jump=make_pair(x,y);
			keep_jumping=true;
			return true;
		}else if(bottom_left_jump(x,y)){
			continued_jump=make_pair(x,y);
			keep_jumping=true;
			return true;
		}else if(bottom_right_jump(x,y)){
			continued_jump=make_pair(x,y);
			keep_jumping=true;
			return true;
			
		}else{
			counter++;
			keep_jumping=false;
			return false;
		}
	}
	return false;

}

int CheckersGame::move_piece(string move){// returns 0 if move was successful, returns -1 if didn't select valid piece, returns -2 must jump,
	//return -3 invalid move, return -4 for jumped, return -5 if crowned.
	bool must_jump = check_jump();
	int player = return_player();

	if(move.length() !=4){//input is wrong
		return -3;
	}

	int x= move[0]-'0';
	int y= move[1]-'0';
	if(x >7 || x<0 || y>7 || y<0){
		return -3; //indicates invalid string, out of bounds
	}
	


	int current_player_piece = checkers.get_value(make_pair(x,y));
	if(player ==1){//checks to see if right player piece selected
		if(current_player_piece ==2|| current_player_piece==4){

		}else{
			return -1;
		}
	}
	if (player ==2){
		if(current_player_piece ==3|| current_player_piece==5){

		}else{
			return -1;
		}
	}

	if(keep_jumping){
		if(x!= continued_jump.first || y!= continued_jump.second){
			return -2;
		}
	}

	string direction(move.substr(2));
	direction[0]=toupper(direction[0]);
	direction[1]=toupper(direction[1]);



	if(direction=="TL"){
		if(x==0||y==0){//impossible move
			return -3;
		}

		if (player==1){
			if(must_jump){
				if(checkers.get_value(make_pair(x-1,y-1))!=3 && checkers.get_value(make_pair(x-1,y-1))!=5){
					return -2;//if a jump is required and there's no piece to jump, invalid move
				}
				if(!top_left_jump(x,y)){
					return -3;
				}

				checkers.set_grid(make_pair(x,y),1);
				checkers.set_grid(make_pair(x-1,y-1),1);
				player2_pieces--;

				if(y-2==0 && current_player_piece==2){
					checkers.set_grid(make_pair(x-2,y-2), 4);
					counter++;
					keep_jumping=false;
					return -5;
				}else{
					checkers.set_grid(make_pair(x-2,y-2), current_player_piece);
				}
				x-=2;//updating position
				y-=2;
				//need to check for additional jumps
				additional_jumps(make_pair(x,y),current_player_piece);
				return -4;

			}

			else if(checkers.get_value(make_pair(x-1,y-1))!=1){
				return -3;//no moves
			
			}else{
				checkers.set_grid(make_pair(x,y),1);
				
				if(y-1==0){//crowning
					checkers.set_grid(make_pair(x-1,y-1), 4);
					counter++;
					return -5;
				}else{
					checkers.set_grid(make_pair(x-1,y-1), current_player_piece);
					counter++;
					return 0;
				}
				
			}

			
		}



		if (player==2){
			if(current_player_piece!= 5){
				return -3;
			}

			if(must_jump){
				if(checkers.get_value(make_pair(x-1,y-1))!=2 && checkers.get_value(make_pair(x-1,y-1))!=4){
					return -2;//if a jump is required and there's no piece to jump, invalid move
				}
				if(!top_left_jump(x,y)){
					return -3;
				}

				checkers.set_grid(make_pair(x,y),1);
				checkers.set_grid(make_pair(x-1,y-1),1);
				player1_pieces--;


				checkers.set_grid(make_pair(x-2,y-2), current_player_piece);
				
				x-=2;//updating position
				y-=2;
				additional_jumps(make_pair(x,y),current_player_piece);
				return -4;


			}else if(checkers.get_value(make_pair(x-1,y-1))==1){
				checkers.set_grid(make_pair(x,y),1);
				checkers.set_grid(make_pair(x-1,y-1), current_player_piece);
				counter++;
				return 0;
			}else{
				return -3;
			}


		}




	}else if(direction=="TR"){
	

		if(x==7||y==0){//impossible move
			return -3;
		}

		if (player==1){
			if(must_jump){
				if(checkers.get_value(make_pair(x+1,y-1))!=3 && checkers.get_value(make_pair(x+1,y-1))!=5){
					return -2;//if a jump is required and there's no piece to jump, invalid move
				}
				if(!top_right_jump(x,y)){
					return -3;
				}

				checkers.set_grid(make_pair(x,y),1);
				checkers.set_grid(make_pair(x+1,y-1),1);
				player2_pieces--;

				if(y-2==0 && current_player_piece==2){
					checkers.set_grid(make_pair(x+2,y-2), 4);
					counter++;
					keep_jumping=false;
					return -5;
				}else{
					checkers.set_grid(make_pair(x+2,y-2), current_player_piece);
				}
				x+=2;//updating position
				y-=2;
				additional_jumps(make_pair(x,y),current_player_piece);
				return -4;

			}

			else if(checkers.get_value(make_pair(x+1,y-1))!=1){
				return -3;//no jumps required, checks to see if square is unoccupied
			
			}else{
				checkers.set_grid(make_pair(x,y),1);
				
				if(y-1==0){//crowning
					checkers.set_grid(make_pair(x+1,y-1), 4);
					counter++;
					return -5;
				}else{
					checkers.set_grid(make_pair(x+1,y-1), current_player_piece);
					counter++;
					return 0;
				}
				
			}

			
		}



		if (player==2){
			if(current_player_piece!= 5){//only a player 2 king could move like this
				return -3;
			}

			if(must_jump){
				if(checkers.get_value(make_pair(x+1,y-1))!=2 && checkers.get_value(make_pair(x+1,y-1))!=4){
					return -2;//if a jump is required and there's no piece to jump, invalid move
				}
				if(!top_right_jump(x,y)){
					return -3;
				}

				checkers.set_grid(make_pair(x,y),1);
				checkers.set_grid(make_pair(x+1,y-1),1);
				player1_pieces--;


				checkers.set_grid(make_pair(x+2,y-2), current_player_piece);
				
				x+=2;//updating position
				y-=2;
				additional_jumps(make_pair(x,y),current_player_piece);
				return -4;



			}else if(checkers.get_value(make_pair(x+1,y-1))==1){
				checkers.set_grid(make_pair(x,y),1);
				checkers.set_grid(make_pair(x+1,y-1), current_player_piece);
				counter++;
				return 0;
			}else{
				return -3;
			}


		}




	}else if(direction=="BR"){

		if(x==7||y==7){//impossible move
			return -3;
		}

		if (player==2){
			if(must_jump){
				if(checkers.get_value(make_pair(x+1,y+1))!=2 && checkers.get_value(make_pair(x+1,y+1))!=4){
					return -2;//if a jump is required and there's no piece to jump, invalid move
				}
				if(!bottom_right_jump(x,y)){
					return -3;
				}

				checkers.set_grid(make_pair(x,y),1);
				checkers.set_grid(make_pair(x+1,y+1),1);
				player1_pieces--;

				if(y+2==0 && current_player_piece==3){
					checkers.set_grid(make_pair(x+2,y+2), 5);
					counter++;
					keep_jumping=false;
					return -5;
				}else{
					checkers.set_grid(make_pair(x+2,y+2), current_player_piece);
				}
				x+=2;//updating position
				y+=2;
				additional_jumps(make_pair(x,y),current_player_piece);
				return -4;


			}

			else if(checkers.get_value(make_pair(x+1,y+1))!=1){
				return -3;//no jumps required, checks to see if square is unoccupied
			
			}else{
				checkers.set_grid(make_pair(x,y),1);
				
				if(y+1==7){//crowning
					checkers.set_grid(make_pair(x+1,y+1), 5);
					counter++;
					return -5;
				}else{
					checkers.set_grid(make_pair(x+1,y+1), current_player_piece);
					counter++;
					return 0;
				}
			}

			
		}



		if (player==1){
			if(current_player_piece!= 4){//only a player 1 king could move like this
				return -3;
			}

			if(must_jump){
				if(checkers.get_value(make_pair(x+1,y+1))!=3 && checkers.get_value(make_pair(x+1,y+1))!=5){
					return -2;//if a jump is required and there's no piece to jump, invalid move
				}
				if(!bottom_right_jump(x,y)){
					return -3;
				}

				checkers.set_grid(make_pair(x,y),1);
				checkers.set_grid(make_pair(x+1,y+1),1);
				player2_pieces--;


				checkers.set_grid(make_pair(x+2,y+2), current_player_piece);
				
				x+=2;//updating position
				y+=2;
				additional_jumps(make_pair(x,y),current_player_piece);
				return -4;



			}else if(checkers.get_value(make_pair(x+1,y+1))==1){
				checkers.set_grid(make_pair(x,y),1);
				checkers.set_grid(make_pair(x+1,y+1), current_player_piece);
				counter++;
				return 0;
			}else{
				return -3;
			}


		}



	}else if(direction=="BL"){
		if(x==0||y==7){//impossible move
			return -3;
		}

		if (player==2){
			if(must_jump){
				if(checkers.get_value(make_pair(x-1,y+1))!=2 && checkers.get_value(make_pair(x-1,y+1))!=4){
					return -2;//if a jump is required and there's no piece to jump, invalid move
				}
				if(!bottom_left_jump(x,y)){
					return -3;
				}

				checkers.set_grid(make_pair(x,y),1);
				checkers.set_grid(make_pair(x-1,y+1),1);
				player1_pieces--;

				if(y+2==7 && current_player_piece==3){
					checkers.set_grid(make_pair(x-2,y+2), 5);
					counter++;
					keep_jumping=false;
					return -5;
				}else{
					checkers.set_grid(make_pair(x-2,y+2), current_player_piece);
				}
				x-=2;//updating position
				y+=2;
				additional_jumps(make_pair(x,y),current_player_piece);
				return -4;


			}

			else if(checkers.get_value(make_pair(x-1,y+1))!=1){
				return -3;//no jumps required, checks to see if square is unoccupied
			
			}else{
				checkers.set_grid(make_pair(x,y),1);
				
				if(y+1==7){//crowning
					checkers.set_grid(make_pair(x-1,y+1), 5);
					counter++;
					return -5;
				}else{
					checkers.set_grid(make_pair(x-1,y+1), current_player_piece);
					counter++;
					return 0;
				}
				
			}

			
		}



		if (player==1){
			if(current_player_piece!= 4){//only a player 1 king could move like this
				return -3;
			}

			if(must_jump){
				if(checkers.get_value(make_pair(x-1,y+1))!=3 && checkers.get_value(make_pair(x-1,y+1))!=5){
					return -2;//if a jump is required and there's no piece to jump, invalid move
				}
				if(!bottom_left_jump(x,y)){
					return -3;
				}

				checkers.set_grid(make_pair(x,y),1);
				checkers.set_grid(make_pair(x-1,y+1),1);
				player2_pieces--;


				checkers.set_grid(make_pair(x-2,y+2), current_player_piece);
				
				x-=2;//updating position
				y+=2;
				additional_jumps(make_pair(x,y),current_player_piece);
				return -4;



			}else if(checkers.get_value(make_pair(x-1,y+1))==1){
				checkers.set_grid(make_pair(x,y),1);
				checkers.set_grid(make_pair(x-1,y+1), current_player_piece);
				counter++;
				return 0;
			}else{
				return -3;
			}

		}

	}else{
		return -3;
	}
	return 0;
}






bool CheckersGame::top_right_jump(int x, int y){
	int player_turn=return_player();
	if (y<=1|| x>=6){
		return false;
	}else{
		if(player_turn==1){
			if (checkers.get_value(make_pair(x+1, y-1))==5 || checkers.get_value(make_pair(x+1, y-1))==3){
				if(checkers.get_value(make_pair(x+2, y-2))==1){
					return true;
				}
			}
		}

		if(player_turn==2){
			if (checkers.get_value(make_pair(x+1, y-1))==4 || checkers.get_value(make_pair(x+1, y-1))==2){
				if(checkers.get_value(make_pair(x+2, y-2))==1){
					return true;
				}
			}
		}
	}
	return false;
}


bool CheckersGame::top_left_jump(int x, int y){
	int player_turn=return_player();
	if (y<=1|| x<=1){
		return false;
	}else{
		if(player_turn==1){
			if (checkers.get_value(make_pair(x-1, y-1))==5 || checkers.get_value(make_pair(x-1, y-1))==3){
				if(checkers.get_value(make_pair(x-2, y-2))==1){
					return true;
				}
			}
		}

		if(player_turn==2){
			if (checkers.get_value(make_pair(x-1, y-1))==4 || checkers.get_value(make_pair(x-1, y-1))==2){
				if(checkers.get_value(make_pair(x-2, y-2))==1){
					return true;
				}
			}
		}
	}
	return false;

}
bool CheckersGame::bottom_right_jump(int x, int y){
	int player_turn=return_player();
	if (y>=6|| x>=6){
		return false;
	}else{
		if(player_turn==1){
			if (checkers.get_value(make_pair(x+1, y+1))==5 || checkers.get_value(make_pair(x+1, y+1))==3){
				if(checkers.get_value(make_pair(x+2, y+2))==1){
					return true;
				}
			}
		}

		if(player_turn==2){
			if (checkers.get_value(make_pair(x+1, y+1))==4 || checkers.get_value(make_pair(x+1, y+1))==2){
				if(checkers.get_value(make_pair(x+2, y+2))==1){
					return true;
				}
			}
		}
	}
	return false;

}
bool CheckersGame::bottom_left_jump(int x, int y){
	int player_turn=return_player();
	if (y>=6|| x<=1){
		return false;
	}else{
		if(player_turn==1){
			if (checkers.get_value(make_pair(x-1, y+1))==5 || checkers.get_value(make_pair(x-1, y+1))==3){
				if(checkers.get_value(make_pair(x-2, y+2))==1){
					return true;
				}
			}
		}

		if(player_turn==2){
			if (checkers.get_value(make_pair(x-1, y+1))==4 || checkers.get_value(make_pair(x-1, y+1))==2){
				if(checkers.get_value(make_pair(x-2, y+2))==1){
					return true;
				}
			}
		}
	}
	return false;

}




int CheckersGame::check_status(){//return 0 is true, 1 if false can move to empty square, 2 if jump is possible
	int player_turn;
	if (counter%2==0){
		player_turn=2;
	}else{
		player_turn=1;
	}
	int move_empty=0;

	if (player_turn==1){//if it's player 1's turn, check to see if there are any moves or jumps available
		for (int i=0; i<8 ; i++){

			for (int j = 0; j < 8; j++){
				
				if (checkers.get_value(make_pair(i,j))==2){//if it's regular player 1 piece
					if(i!=0 && j!=0){//checking up,left position

						if(checkers.get_value(make_pair(i-1,j-1))==1){//seeing if it's empty square, if it is, no draw
							move_empty=1;
						}

						if(top_left_jump(i,j)){
							return 2;
						}
					}



					if(i!=7 && j!=0){//checking up, right move
						if(checkers.get_value(make_pair(i+1,j-1))==1){//seeing if it's empty square, if it is, no draw
							move_empty=1;
						}

						if(top_right_jump(i,j)){
							return 2;
						}
					}


				}else if(checkers.get_value(make_pair(i,j))==4){//if it's player 1 king
					
					if(i!=0 && j!=0 ){//checking up,left position, 
						if(checkers.get_value(make_pair(i-1,j-1))==1){//seeing if it's empty square, if it is, no draw
							move_empty=1;
						}

						if(top_left_jump(i,j)){
							return 2;
						}
					}



					if(i!=7 && j!=0){//checking up, right move
						if(checkers.get_value(make_pair(i+1,j-1))==1){//seeing if it's empty square, if it is, no draw
							move_empty=1;
						}
						if(top_right_jump(i,j)){
							return 2;
						}
					}

					if(i!=0 && j!=7){//checking down, left move
						if(checkers.get_value(make_pair(i-1,j+1))==1){//seeing if it's empty square, if it is, no draw
							move_empty=1;
						}

						if(bottom_left_jump(i,j)){
							return 2;
						}
					}


					if(i!=7 && j!=7){//checking down, right move
						if(checkers.get_value(make_pair(i+1,j+1))==1){//seeing if it's empty square, if it is, no draw
							move_empty=1;
						}

						if(bottom_right_jump(i,j)){
							return 2;
						}
					}
				}
			}
		}
	}






	else if(player_turn==2){//if it's player 2's turn, check to see if there are any moves or jumps available
		for (int i=0; i<8 ; i++){

			for (int j = 0; j < 8; j++){
				
				if (checkers.get_value(make_pair(i,j))==3){//if it's regular player 2 piece
					
					if(i!=0 && j!=7){//checking down, left move
						if(checkers.get_value(make_pair(i-1,j+1))==1){//seeing if it's empty square, if it is, no draw
							move_empty=1;
						}
						if(bottom_left_jump(i,j)){
							return 2;
						}
					}


					if(i!=7 && j!=7){//checking down, right move
						if(checkers.get_value(make_pair(i+1,j+1))==1){//seeing if it's empty square, if it is, no draw
							move_empty=1;
						}
						if(bottom_right_jump(i,j)){
							return 2;
						}
					}


				}else if(checkers.get_value(make_pair(i,j))==5){//if it's player 2 king
					if(i!=0 && j!=7){//checking bottom,left position

						if(checkers.get_value(make_pair(i-1,j+1))==1){//seeing if it's empty square, if it is, no draw
							move_empty=1;
						}

						if(bottom_left_jump(i,j)){
							return 2;
						}
					}



					if(i!=7 && j!=7){//checking bottom, right move
						if(checkers.get_value(make_pair(i+1,j+1))==1){//seeing if it's empty square, if it is, no draw
							move_empty=1;
						}

						if(bottom_right_jump(i,j)){
							return 2;
						}
					}
					if(i!=0 && j!=0){//checking up, left move
						if(checkers.get_value(make_pair(i-1,j-1))==1){//seeing if it's empty square, if it is, no draw
							move_empty=1;
						}
						if(top_left_jump(i,j)){
							return 2;
						}
					}


					if(i!=7 && j!=0){//checking up, right move
						if(checkers.get_value(make_pair(i+1,j-1))==1){//seeing if it's empty square, if it is, no draw
							move_empty=1;
						}
						if(top_right_jump(i,j)){
							return 2;
						}
					}

				}
			}
		}
	}


	
	return move_empty;
}

void CheckersGame::set_game_status(){

	if (player1_pieces==0){//player 2 wins.
		game_status=RESULT_PLAYER2_WINS;
		game_over=2;
	}else if(player2_pieces==0){//player 1 wins
		game_status=RESULT_PLAYER1_WINS;
		game_over=1;
	}else if(check_status()==0){
		game_status=RESULT_STALEMATE;
		game_over=3;
	}else{
		game_status=RESULT_KEEP_PLAYING;
		game_over=0;
	}
}

GameResult CheckersGame::return_game_status(){

	return game_status;
}

void CheckersGame::setCheckersBoard(){
	player1_pieces = 12;
	player2_pieces=12;
	keep_jumping=false;
	continued_jump=make_pair(0,0);

	for (int i = 0; i < 8; i++){//all white squares
		for(int j=0; j<8; j++){
			checkers.set_grid(make_pair(i,j),0);
		}
	}

	for(int i =0; i< 8; i++){

		if(i%2==0){
			checkers.set_grid(make_pair(i, 1),1);
			checkers.set_grid(make_pair(i, 3),1);
			checkers.set_grid(make_pair(i, 5),1);
			checkers.set_grid(make_pair(i, 7),1);
		}else{
			checkers.set_grid(make_pair(i, 0),1);
			checkers.set_grid(make_pair(i, 2),1);
			checkers.set_grid(make_pair(i, 4),1);
			checkers.set_grid(make_pair(i, 6),1);
		}
	}
		
	for (int i=1; i<8 ;i+=2){
		checkers.set_grid(make_pair(i, 0),3);
		checkers.set_grid(make_pair(i, 2),3);
		checkers.set_grid(make_pair(i, 6),2);
	}

	for (int i=0; i<8; i+=2){
		checkers.set_grid(make_pair(i, 1),3);
		checkers.set_grid(make_pair(i, 5),2);
		checkers.set_grid(make_pair(i, 7),2);

	}
}

ostream & operator<<(ostream & out, const CheckersGame & checker_board){
    //out<<"Board:\n"<<checker_board.checkers; //comment out for no print
    out<<(Game &) checker_board;
    return out;
}

void CheckersGame::setup_draw_test(){
	for (int i = 0; i < 8; i++){//all white squares
		for(int j=0; j<8; j++){
			checkers.set_grid(make_pair(i,j),0);
		}
	}

	for(int i =0; i< 8; i++){

		if(i%2==0){
			checkers.set_grid(make_pair(i, 1),1);
			checkers.set_grid(make_pair(i, 3),1);
			checkers.set_grid(make_pair(i, 5),1);
			checkers.set_grid(make_pair(i, 7),1);
		}else{
			checkers.set_grid(make_pair(i, 0),1);
			checkers.set_grid(make_pair(i, 2),1);
			checkers.set_grid(make_pair(i, 4),1);
			checkers.set_grid(make_pair(i, 6),1);
		}
	}
		
	for (int i=1; i<8 ;i+=2){
		checkers.set_grid(make_pair(i, 1),3);
		checkers.set_grid(make_pair(i, 3),3);
		checkers.set_grid(make_pair(i, 5),2);
	}

	for (int i=0; i<8; i+=2){
		checkers.set_grid(make_pair(i, 2),3);
		checkers.set_grid(make_pair(i, 4),2);
		checkers.set_grid(make_pair(i, 6),2);

	}

}
