
//  battleship.cpp
//  hw5
//
//  Created by HuangXinyu on 4/12/16.
//

#include "battleship.h"

using namespace std;

//attack a square
//return RESULT_INVALID if the coordinate given is out of boundary
//or it has been attacked before
//return RESULT_PLAYER1_WINS if player 1 wins
//return RESULT_PLAYER2_WINS if player 2 wins
//return RESULT_KEEP_PLAYING if the game has not ended
GameResult BattleshipGame::attack_square(Coord coordinate){
    int x = coordinate.first;
    int y = coordinate.second;
    //check if the game has ended
    if (game_over)
	return  RESULT_INVALID;
    //check if the coordinates given is out of boundary
    if (x>9 || y>9 || x<0 || y<0){
        return RESULT_INVALID;
    }
    int player_num = return_player();
    counter++;
    //if the grid has been attacked before
    int temp = check_miss(player_num-1,make_pair(x,y));
    GameResult result=RESULT_KEEP_PLAYING;
    string printStat = "HIT\n";
    if (temp){
        int curr = battleship[player_num-1].get_value(coordinate);
        if (curr > 0)
            battleship[player_num-1].set_grid(coordinate, -1*curr);
    	if (check_sunk(player_num-1,temp-1)){
            printStat = "SUNK " + ships[temp-1].first + "\n";
    	}
        result = check_status();
        if (game_over){
            return result;
        }
	out<<printStat;
    }
    else{
	out<<"MISS\n";
    }
    return result;
}

//initialize the board
//player 1 sets board 1 and player 2 sets board 0
void BattleshipGame::setBattleship(int playerNum){
    status location;
    for (int i=0;i<5;i++){
	bool flag = false; //flag is true if the placement of ship is successfull
	while (!flag){
            location = prompt(playerNum,i);
	    flag = checkValid(ships[i].second,1-playerNum,location);
	    if (flag == false){
		out<<"INVALID MOVE\n";
	    }
	}
	ship_status[1-playerNum][i] = location;
	placeShip(1-playerNum,i,location);
    }
}


//prompting the user for input for constructor
//playerNum is 0 or 1
//shipNum is from 0 to 4
status BattleshipGame::prompt(int playerNum, int shipNum) {
    string place;
    bool user_input = false; //user_input is false when the user uses the wrong format of input
    while (!user_input){
        out<<"PLAYER "<<playerNum+1<<" PLACE "<<ships[shipNum].first<<":";
        getline(in,place);
	if (place == "q"){
	    throw "quit";
	}
	user_input = checkUserInput(place);
	if (!user_input){
	    out<<"INVALID MOVE\n";
	}
    }
    int x = place[0]-'0';
    int y = place[1]-'0';
    char direction = place[2];
    status temp = {make_pair(x,y),direction};
    return temp;
}

//placing a ship
//playerNum is 0 or 1
//sets board[playerNum]
//shipNum is 0 to 4
void BattleshipGame::placeShip(int playerNum, int shipNum, status location) {
    int x = location.coordinate.first;
    int y = location.coordinate.second;
    char direction = location.orientation;
    for (int i=0;i<ships[shipNum].second;i++){
	battleship[playerNum].set_grid(make_pair(x,y),shipNum+1);
	if (direction == 'h'){
	    x++;
	}
	else{
	    y++;
	}
    }
}

//removes a ship from grid and returns values removed
vector<int> BattleshipGame::removeShip(int playerNum, status ship) {
    
    Coord start = ship.coordinate;
    char orient = ship.orientation;
    int shipNum = battleship[playerNum].get_value(start);
    if (shipNum < 0)
        shipNum*=-1;
    int spaces = ships[shipNum-1].second;
    
    vector<int> toReturn;
    Coord newCoord = make_pair(start.first,start.second);

    for (int a=0; a<spaces; a++){
        
        int x = newCoord.first;
        int y = newCoord.second;

        int temp = battleship[playerNum].get_value(newCoord);

        toReturn.push_back(temp);

        battleship[playerNum].set_grid(newCoord,0);
                                           
        if(orient=='h')
            x++;
        else
            y++;
        
        newCoord=make_pair(x,y);
        
    }
    return toReturn;
}

//placing check user input into a different method
bool BattleshipGame::checkUserInput(string place) {

    if (place.length() < 3 || place.length() > 3) {
        return false;
    }

    regex rgx("[0-9][0-9]((v)|(h))");
    smatch match;
    
    if (regex_search(place, match, rgx)) {
        return true;
    }
    return false;
}

//checks if a ship is valid in constructor in Board[player]
//player is 0 or 1
bool BattleshipGame::checkValid(int shipLength, int player, status location) {
    
    char orient = location.orientation;
    Coord coordinate = location.coordinate;
    
    Coord newCoord = make_pair(coordinate.first,coordinate.second);
    
    for (int a=0; a<shipLength; a++) {

        int x = newCoord.first;
        int y = newCoord.second;
        if (x>9 || y>9 || x<0 || y<0 || battleship[player].get_value(newCoord)!=0){
            return false;
        }
        
        if (orient=='h') {
            x++;
        } else {
            y++;
        }
        newCoord=make_pair(x,y);
    }
    return true;
}

//check whether a game ends or not
//return keep playing or which player wins
GameResult BattleshipGame::check_status(void){
    bool flag1 = false;//false means player 1 wins
    bool flag2 = false;//false means player 2 wins
    for (int i=0;i<10;i++){
        for (int j=0;j<10;j++){
            if (battleship[0].get_value(make_pair(i,j)) > 0)
                flag1 = true;
            if (battleship[1].get_value(make_pair(i,j)) > 0)
                flag2 = true;
        }
    }
    if (!flag1){
        game_over = 1;
        return RESULT_PLAYER1_WINS;
    }
    if (!flag2){
	game_over = 2;
        return RESULT_PLAYER2_WINS;
    }
    return RESULT_KEEP_PLAYING;
}

//return false if no ship is sunk
//return true if shipNum is sunk
//shipNum is 0 to 4
bool BattleshipGame::check_sunk(int player_num,int shipNum){
    int x = ship_status[player_num][shipNum].coordinate.first;
    int y = ship_status[player_num][shipNum].coordinate.second;
    char orient = ship_status[player_num][shipNum].orientation;
    int length = ships[shipNum].second;
    int i = 0;
    int flag = true; //true means the ship is sunk
    while (i<length){
	if (battleship[player_num].get_value(make_pair(x,y)) > 0){
	    flag = false;
	    break;
	}
	if (orient == 'h')
	    x++;
	else
	    y++;
	i++;
    }
    if (flag){
        removeShip(player_num,ship_status[player_num][shipNum]);
        ship_status[player_num][shipNum].orientation = 's';
    }
    return flag;
}

//print out two Boards and which player is going to play
ostream & operator<<(ostream & out, const BattleshipGame & bs){
    out<<"Board 1:\n"<<bs.battleship[0];
    out<<"Board 2:\n"<<bs.battleship[1];
    out<<(Game &) bs;
    return out;
}

int BattleshipGame::check_miss(int player, Coord coordinate){

    int x = coordinate.first;
    int y = coordinate.second;
    for (int i = 0;i<5;i++){
	int ship_x = ship_status[player][i].coordinate.first;
	int ship_y = ship_status[player][i].coordinate.second;
	char orient = ship_status[player][i].orientation;
	for (int j = 0;j<ships[i].second;j++){
	    if (ship_x == x && ship_y == y && ship_status[player][i].orientation != 's'){
		return i+1;
	    }
	    if (orient == 'h'){
		ship_x++;
	    }
	    else{
		ship_y++;
	    }
	}
    }
    return 0;

}
