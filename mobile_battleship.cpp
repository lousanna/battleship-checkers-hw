//
//  mobile_battleship.cpp
//  hw5
//
//  Created by Lousanna on 4/24/16.
//

#include "mobile_battleship.h"

using namespace std;

//checks if a valid attack or movement
bool MobileBSGame::validMove(string input) {
    
    int x = input[0]-'0';
    int y = input[1]-'0';
    
    //Coordinate has to be in bounds.
    if (x>9 || y>9 || x<0 || y<0){
        return false;
    }

    int currPlayer = return_player() - 1; //Board to attack from current player
    
    if(input.size() == 4) { //move ship
        
        char direction = input[2];
        int spaces = input[3]-'0';
        
        //Only up, down, left, or right
        if (direction != 'u' && direction != 'd' && direction != 'l' && direction != 'r') {
            return false;
        }
        
        //Only accept moves of 1, 2, or 3 spaces
        if (!isdigit(input[3]) || spaces <= 0 || spaces > 3) {
            return false;
        }
        
        //Move has to be in bounds and within a free path.
        Coord choice = make_pair(x,y);
        int shipNum = getShip(1-currPlayer, choice);
	
        if (shipNum <0){ //no ship
            return false;
        }

        Coord old = ship_status[1-currPlayer][shipNum].coordinate; //gets first coordinate of ship
        char orient = ship_status[1-currPlayer][shipNum].orientation;

        if (orient=='s') //check if there is no ship at space
            return false;
	
        //check if the orientation of move is the same as ship
        if (orient == 'h' && (direction == 'd' || direction == 'u')){
            return false;
        }
        if (orient == 'v' && (direction == 'l' || direction == 'r')){
            return false;
        }

	    for (int a=1; a<=spaces; a++) { //check if path is free
            	Coord checkSpace = moveTo(direction, a, old);
            	status location = {checkSpace, orient};

                if(getShip(1-currPlayer, checkSpace)!= shipNum && getShip(1-currPlayer,checkSpace)!=-1 && !checkValid(shipNum,1-currPlayer, location))
                	return false;
            }
    }
    return true;
}

//returns the number value of the ship at a given coordinate
int MobileBSGame::getShip(int currPlayer, Coord choice) {
    int shipNum = battleship[currPlayer].get_value(choice);
    
    if(shipNum < 0)
        shipNum*=-1;

    return shipNum-1;
}

//returns the new coordinate to move to
Coord MobileBSGame::moveTo(char direction, int spaces, Coord old) {
    int x = old.first;
    int y = old.second;
    
    if (direction == 'u')
        y-= spaces;
    else if (direction == 'd')
        y+= spaces;
    else if (direction == 'l')
        x-= spaces;
    else
        x+= spaces;
    
    return make_pair(x,y);
}

//moves a ship
void MobileBSGame::moveShip(int playerNum, vector<int> shipNum, status location) {
    int x = location.coordinate.first;
    int y = location.coordinate.second;
    char direction = location.orientation;

    for (size_t i=0;i<shipNum.size();i++){
        battleship[playerNum].set_grid(make_pair(x,y),shipNum[i]);
        if (direction == 'h'){
            x++;
        }
        else{
            y++;
        }
    }
}

//Overrides battleship's attack square to allow for moving ships
GameResult MobileBSGame::attack_square(string input) {
    
    if (game_over)
        return  RESULT_INVALID;

    Coord coordinate = make_pair(input[0]-'0',input[1]-'0');
    int currPlayer = 2 - return_player();
    
    if (input.size() == 2) {//call super class
        return BattleshipGame::attack_square(coordinate);
    }
    
    if (input.size() == 4){
    	if(!validMove(input)){
            return RESULT_INVALID;
        }
	
        char direction = input[2];
        int spaces = input[3]-'0';
        
        int shipNum = getShip(currPlayer, coordinate);

        status currShip = ship_status[currPlayer][shipNum];
        char orient = currShip.orientation;
        vector<int> p = removeShip(currPlayer, currShip); //remove ship
        
        status newLoc = {moveTo(direction, spaces, currShip.coordinate), orient};
        moveShip(currPlayer, p, newLoc);
        ship_status[currPlayer][shipNum]=newLoc;
        
        counter++;
        return RESULT_KEEP_PLAYING; 
    }
    return RESULT_INVALID;
}
