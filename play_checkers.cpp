/*Final Project play_checkers.cpp, 600.120 Spring 2016
 *
 *
 *Peiyong(Steven) Zhang, 600.120, 4/27/15,Final Project, 4435781967, pzhang18, pzhang18@jhu.edu
 */


#include "checkers.h"
#include <cassert>
#include <string>
using namespace std;

void game1(void);
void game2(void);
void game3(void);
void game4(void);
void game5(void);
int main (void){
    game1();
	game2();
	game3();
	game4();
	game5();

	cout << "All tests passed" << endl;
    return 0;
}
void game1(){//player 2 wins, contains double jumps and a triple
	CheckersGame checker_board = CheckersGame();



	assert(checker_board.move_piece("45tl")==0);
	assert(checker_board.move_piece("52Bl")==0);
	assert(checker_board.move_piece("34tl")==-2);//need to jump
	assert(checker_board.move_piece("34tr")==-4);//jumped
	assert(checker_board.move_piece("41br")==-4);//jumped
	assert(checker_board.move_piece("65tr")==0);
	assert(checker_board.move_piece("50bl")==0);
	assert(checker_board.move_piece("05tl")==-3);//invalid move
	assert(checker_board.move_piece("12bl")==-1);//selected wrong player piece
	assert(checker_board.move_piece("74tl")==-4);//jumped
	assert(checker_board.move_piece("41br")==-4);//jumped
	assert(checker_board.move_piece("25tr")==0);
	assert(checker_board.move_piece("12br")==0);
	assert(checker_board.move_piece("34tl")==-4);//jumped
	assert(checker_board.move_piece("01br")==-4);//jumped
	assert(checker_board.move_piece("36tr")==0);
	assert(checker_board.move_piece("21bl")==0);
	assert(checker_board.move_piece("16tr")==0);
	assert(checker_board.move_piece("63bl")==0);
	assert(checker_board.move_piece("45tr")==-4);//jumped
	assert(checker_board.move_piece("72bl")==-4);//jumped
	assert(checker_board.move_piece("25tl")==0);
	assert(checker_board.move_piece("10br")==0);
	assert(checker_board.move_piece("56tr")==0);
	assert(checker_board.move_piece("05tr")==-1);//selected wrong player
	assert(checker_board.move_piece("30bl")==-3);//invalid move
	assert(checker_board.move_piece("54bl")==0);
	assert(checker_board.move_piece("14tl")==0);
	assert(checker_board.check_status()==1);
	assert(checker_board.move_piece("23br")==0);
	assert(checker_board.move_piece("47tl")==0);
	assert(checker_board.move_piece("32br")==0);
	assert(checker_board.move_piece("36tl")==-2);//had to jump but didn't
	assert(checker_board.check_status()==2);//indicates current player must jump
	assert(checker_board.move_piece("36tr")==-4);//jump
	assert(checker_board.move_piece("54tl")==-4);//jump
	assert(checker_board.move_piece("32tl")==-5);//triple jump into crown
	assert(checker_board.move_piece("34br")==0);
	assert(checker_board.move_piece("03tr")==-4);//jump
	assert(checker_board.move_piece("30bl")==-4);//jump
	assert(checker_board.move_piece("05tr")==0);
	assert(checker_board.move_piece("61bl")==0);
	assert(checker_board.move_piece("07tr")==0);
	assert(checker_board.move_piece("12bl")==0);
	assert(checker_board.move_piece("27tr")==0);
	assert(checker_board.move_piece("70br")==-3);//bad
	assert(checker_board.move_piece("45br")==-2);//need to jump
	assert(checker_board.move_piece("45bl")==-5);//jumped and crowned
	assert(checker_board.move_piece("65tl")==0);
	assert(checker_board.move_piece("27tl")==-4); //jump
	assert(checker_board.move_piece("05tr")==-4);//jump
	assert(checker_board.move_piece("67tl")==0);
	assert(checker_board.move_piece("23tr")==0);
	assert(checker_board.move_piece("76tl")==0);
	assert(checker_board.move_piece("70bl")==0);
	assert(checker_board.move_piece("65tr")==0);
	assert(checker_board.move_piece("52Bl")==0);
	assert(checker_board.move_piece("10br")==0);
	assert(checker_board.move_piece("43br")==-4);//jump
	assert(checker_board.move_piece("65bl")==-5);//crown
	assert(checker_board.move_piece("21br")==-4);//jump
	assert(checker_board.move_piece("47tr")==0);
	assert(checker_board.move_piece("74tl")==0);
	assert(checker_board.move_piece("56tl")==0);
	assert(checker_board.move_piece("43bl")==0);
	assert(checker_board.move_piece("45tl")==-4);//jump
	assert(checker_board.move_piece("63tl")==0);
	assert(checker_board.move_piece("61bl")==-4);//jump
	checker_board.set_game_status();
	assert(checker_board.return_game_status()==RESULT_PLAYER2_WINS);//check that player 1 has won
	cout << "game1 passed" << endl;
}


void game2(){//player 2 wins 
	CheckersGame checker_board = CheckersGame();

	//-1,-2,-3 mean invalid moves, -4, is jump, -5 is crown
	assert(checker_board.move_piece("05tr")==0);
	assert(checker_board.move_piece("24bl")==-1);
	assert(checker_board.move_piece("12bl")==-0);
	assert(checker_board.move_piece("65tr")==-0);
	assert(checker_board.move_piece("65tr")==-1);
	assert(checker_board.move_piece("65tr")==-1);
	assert(checker_board.move_piece("21bl")==0);
	assert(checker_board.move_piece("76tl")==0);
	assert(checker_board.move_piece("30bl")==0);
	assert(checker_board.move_piece("16tl")==0);
	assert(checker_board.move_piece("52br")==0);
	assert(checker_board.move_piece("14tl")==-3);
	assert(checker_board.move_piece("14tr")==-2);
	assert(checker_board.move_piece("75tl")==-1);
	assert(checker_board.move_piece("74tr")==-3);
	assert(checker_board.move_piece("74tl")==-4);
	assert(checker_board.move_piece("52tl")==-5);
	assert(checker_board.move_piece("72bl")==0);
	assert(checker_board.move_piece("07tr")==0);
	assert(checker_board.move_piece("50bl")==0);
	assert(checker_board.move_piece("65tr")==-2);
	assert(checker_board.move_piece("30br")==-4);
	assert(checker_board.move_piece("52br")==-4);
	assert(checker_board.move_piece("32br")==0);
	assert(checker_board.move_piece("25tr")==0);
	assert(checker_board.move_piece("43bl")==-4);
	assert(checker_board.move_piece("25bl")==-5);
	assert(checker_board.move_piece("74tl")==0);
	assert(checker_board.move_piece("07tr")==-2);
	assert(checker_board.move_piece("03br")==-4);
	assert(checker_board.move_piece("36tl")==-4);
	assert(checker_board.move_piece("21br")==0);
	assert(checker_board.move_piece("65tl")==0);
	assert(checker_board.move_piece("32br")==0);
	assert(checker_board.move_piece("54tl")==-4);
	assert(checker_board.move_piece("12bl")==0);
	assert(checker_board.move_piece("32tr")==0);
	assert(checker_board.move_piece("03br")==-4);
	assert(checker_board.move_piece("27tr")==0);
	assert(checker_board.move_piece("25bl")==0);
	assert(checker_board.move_piece("41tr")==-5);
	assert(checker_board.move_piece("16br")==-5);
	assert(checker_board.move_piece("50br")==-4);
	assert(checker_board.move_piece("01br")==0);
	assert(checker_board.move_piece("67tr")==0);
	assert(checker_board.move_piece("10br")==0);
	assert(checker_board.move_piece("45tr")==0);
	assert(checker_board.move_piece("07tr")==-2);
	assert(checker_board.move_piece("27tr")==-4);
	assert(checker_board.move_piece("45tr")==-3);
	assert(checker_board.move_piece("45br")==-4);
	assert(checker_board.move_piece("47tr")==0);
	assert(checker_board.move_piece("67tl")==-4);
	assert(checker_board.move_piece("63tl")==0);
	assert(checker_board.move_piece("45tr")==-4);
	assert(checker_board.move_piece("63tl")==-4);
	assert(checker_board.move_piece("72tl")==0);
	assert(checker_board.move_piece("70bl")==-4);
	assert(checker_board.move_piece("76tl")==0);
	assert(checker_board.move_piece("52br")==0);
	assert(checker_board.move_piece("65tl")==0);
	assert(checker_board.move_piece("63bl")==-4);
	assert(checker_board.move_piece("05tr")==0);
	assert(checker_board.move_piece("41tr")==0);
	assert(checker_board.move_piece("14tr")==0);
	assert(checker_board.move_piece("12br")==-4);
	checker_board.set_game_status();
	assert(checker_board.return_game_status()==RESULT_PLAYER2_WINS); //check that player 2 has won
	cout<< "game2 passed" <<endl;
}


void game3(){//generates a situation where a player can't move ,player 1 can't move

	CheckersGame checker_board = CheckersGame();
	checker_board.setup_draw_test();//sets up a no move situation
	checker_board.set_game_status();
	assert(checker_board.return_game_status()==RESULT_STALEMATE);
	assert(checker_board.check_status()==0);//no moves possible for player 1
	cout <<"game3 passed" << endl;
}


void game4(){//this is the exact copy of game2, except at the end, player 2 can't move on his turn so it's a "draw"
	CheckersGame checker_board = CheckersGame();
	assert(checker_board.move_piece("05tr")==0);
	assert(checker_board.move_piece("24bl")==-1);
	assert(checker_board.move_piece("12bl")==-0);
	assert(checker_board.move_piece("65tr")==-0);
	assert(checker_board.move_piece("65tr")==-1);
	assert(checker_board.move_piece("65tr")==-1);
	assert(checker_board.move_piece("21bl")==0);
	assert(checker_board.move_piece("76tl")==0);
	assert(checker_board.move_piece("30bl")==0);
	assert(checker_board.move_piece("16tl")==0);
	assert(checker_board.move_piece("52br")==0);
	assert(checker_board.move_piece("14tl")==-3);
	assert(checker_board.move_piece("14tr")==-2);
	assert(checker_board.move_piece("75tl")==-1);
	assert(checker_board.move_piece("74tr")==-3);
	assert(checker_board.move_piece("74tl")==-4);
	assert(checker_board.move_piece("52tl")==-5);
	assert(checker_board.move_piece("72bl")==0);
	assert(checker_board.move_piece("07tr")==0);
	assert(checker_board.move_piece("50bl")==0);
	assert(checker_board.move_piece("65tr")==-2);
	assert(checker_board.move_piece("30br")==-4);
	assert(checker_board.move_piece("52br")==-4);
	assert(checker_board.move_piece("32br")==0);
	assert(checker_board.move_piece("25tr")==0);
	assert(checker_board.move_piece("43bl")==-4);
	assert(checker_board.move_piece("25bl")==-5);
	assert(checker_board.move_piece("74tl")==0);
	assert(checker_board.move_piece("07tr")==-2);
	assert(checker_board.move_piece("03br")==-4);
	assert(checker_board.move_piece("36tl")==-4);
	assert(checker_board.move_piece("21br")==0);
	assert(checker_board.move_piece("65tl")==0);
	assert(checker_board.move_piece("32br")==0);
	assert(checker_board.move_piece("54tl")==-4);
	assert(checker_board.move_piece("12bl")==0);
	assert(checker_board.move_piece("32tr")==0);
	assert(checker_board.move_piece("03br")==-4);
	assert(checker_board.move_piece("27tr")==0);
	assert(checker_board.move_piece("25bl")==0);
	assert(checker_board.move_piece("41tr")==-5);
	assert(checker_board.move_piece("16br")==-5);
	assert(checker_board.move_piece("50br")==-4);
	assert(checker_board.move_piece("01br")==0);
	assert(checker_board.move_piece("67tr")==0);
	assert(checker_board.move_piece("10br")==0);
	assert(checker_board.move_piece("45tr")==0);
	assert(checker_board.move_piece("07tr")==-2);
	assert(checker_board.move_piece("27tr")==-4);
	assert(checker_board.move_piece("45tr")==-3);
	assert(checker_board.move_piece("45br")==-4);
	assert(checker_board.move_piece("47tr")==0);
	assert(checker_board.move_piece("67tl")==-4);
	assert(checker_board.move_piece("63tl")==0);
	assert(checker_board.move_piece("45tr")==-4);
	assert(checker_board.move_piece("63tl")==-4);
	assert(checker_board.move_piece("72tl")==0);
	assert(checker_board.move_piece("70bl")==-4);
	assert(checker_board.move_piece("76tl")==0);
	assert(checker_board.move_piece("52br")==0);
	assert(checker_board.move_piece("65tl")==0);
	assert(checker_board.move_piece("63bl")==-4);
	assert(checker_board.move_piece("05tr")==0);
	assert(checker_board.move_piece("41tr")==0);
	assert(checker_board.move_piece("14tl")==0);
	assert(checker_board.move_piece("45br")==0);
	assert(checker_board.check_status()==0);
	checker_board.set_game_status();
	assert(checker_board.return_game_status()==RESULT_STALEMATE);
	cout<< "game4 passed" <<endl;


}

void game5(){//player 1 wins, contains a legendary quad jump


	CheckersGame checker_board = CheckersGame();

	//-1,-2,-3 mean invalid moves, -4, is jump, -5 is crown
	assert(checker_board.move_piece("65tr")==0);
	assert(checker_board.move_piece("74tr")==-1);
	assert(checker_board.move_piece("72bl")==0);
	assert(checker_board.move_piece("74tr")==-3);
	assert(checker_board.move_piece("05tr")==0);
	assert(checker_board.move_piece("61br")==0);
	assert(checker_board.move_piece("76tl")==0);
	assert(checker_board.move_piece("50br")==0);
	assert(checker_board.move_piece("67tr")==0);
	assert(checker_board.move_piece("32bl")==0);
	assert(checker_board.move_piece("14tr")==-4);
	assert(checker_board.move_piece("32tr")==-5);
	assert(checker_board.move_piece("63bl")==0);
	assert(checker_board.move_piece("45tr")==-4);
	assert(checker_board.move_piece("63tl")==-4);
	assert(checker_board.move_piece("30br")==-4);
	assert(checker_board.move_piece("25tl")==0);
	assert(checker_board.move_piece("12br")==0);
	assert(checker_board.move_piece("14tl")==-2);
	assert(checker_board.move_piece("14tr")==-4);
	assert(checker_board.move_piece("21br")==-4);
	assert(checker_board.move_piece("16tr")==0);
	assert(checker_board.move_piece("10br")==0);
	assert(checker_board.move_piece("25tl")==0);
	assert(checker_board.move_piece("72bl")==0);
	assert(checker_board.move_piece("50br")==-4);//oh baby a quadra kill
	assert(checker_board.move_piece("72tl")==-2);
	assert(checker_board.move_piece("72bl")==-4);
	assert(checker_board.move_piece("54tl")==-4);
	assert(checker_board.move_piece("32tl")==-4);
	assert(checker_board.move_piece("70bl")==0);
	assert(checker_board.move_piece("10br")==0);
	assert(checker_board.move_piece("52br")==0);
	assert(checker_board.move_piece("74tl")==-4);
	assert(checker_board.move_piece("52tr")==-5);
	assert(checker_board.move_piece("01br")==0);
	assert(checker_board.move_piece("21bl")==-4);
	checker_board.set_game_status();
	assert(checker_board.return_game_status()==RESULT_PLAYER1_WINS); //check that player 2 has won
	cout<< "game5 passed" <<endl;
}