CXX = g++
CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CXXFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

.PHONY: all
all: main play_bs play_mobile_bs play_checkers

play_bs.o: play_bs.cpp battleship.h
	$(CXX) -c play_bs.cpp $(CXXFLAGS)

play_bs: play_bs.o battleship.o battleship.h
	$(CXX) -o play_bs play_bs.o battleship.o

play_mobile_bs.o: play_mobile_bs.cpp mobile_battleship.h
	$(CXX) -c play_mobile_bs.cpp $(CXXFLAGS)

play_mobile_bs: play_mobile_bs.o mobile_battleship.o mobile_battleship.h
	$(CXX) -o play_mobile_bs play_mobile_bs.o mobile_battleship.o battleship.o

play_checkers.o: play_checkers.cpp checkers.h
	$(CXX) -c play_checkers.cpp $(CXXFLAGS)

play_checkers: play_checkers.o checkers.o checkers.h
	$(CXX) -o play_checkers play_checkers.o checkers.o

battleship.o: battleship.cpp battleship.h
	$(CXX) -c battleship.cpp $(CXXFLAGS)

mobile_battleship.o: mobile_battleship.cpp mobile_battleship.h
	$(CXX) -c mobile_battleship.cpp $(CXXFLAGS)

checkers.o: checkers.cpp checkers.h
	$(CXX) -c checkers.cpp $(CXXFLAGS)

main.o: main.cpp battleship.h mobile_battleship.h checkers.h
	$(CXX) -c main.cpp $(CXXFLAGS)

main: main.o battleship.o mobile_battleship.o checkers.o
	$(CXX) -o main main.o battleship.o mobile_battleship.o checkers.o 

clean:
	rm -f *.o main play_mobile_bs play_checkers play_bs
