#include "battleship.h"

class MobileBSGame:public BattleshipGame{
public:
    MobileBSGame(std::ostream &output = std::cout,std::istream &input = std::cin):BattleshipGame(output, input){}
    bool validMove(std::string input);
    virtual GameResult attack_square(std::string input);
    
protected:
    Coord moveTo(char direction, int spaces, Coord old);
    void moveShip(int playerNum, std::vector<int> p, status location);
    int getShip(int player, Coord choice);
};
