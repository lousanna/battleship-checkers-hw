/*Final Project play_checkers.cpp, 600.120 Spring 2016
 *
 *
 *Peiyong(Steven) Zhang, 600.120, 4/27/15,Final Project, 4435781967, pzhang18, pzhang18@jhu.edu
 */

#ifndef CHECKERS_H
#define CHECKERS_H

#include "game.h"
#include <string>
#include <array>


class CheckersGame:public Game{
public:
	CheckersGame(){
		setCheckersBoard();
	}
	bool additional_jumps(Coord, int);
	int move_piece(std::string move);
	void set_game_status();
	GameResult return_game_status();
   	int check_status();
	bool check_jump();
	bool top_right_jump(int x, int y);
	bool top_left_jump(int x, int y);
	bool bottom_right_jump(int x , int y);
	bool bottom_left_jump(int x, int y);
	void setCheckersBoard();
 	void setup_draw_test();
	friend std::ostream& operator<<(std::ostream & out, const CheckersGame & checker_board);
private:	
	Board checkers=Board(8);
	bool keep_jumping;
	Coord continued_jump;
	GameResult game_status;
	int player1_pieces; //player 1 pieces
	int player2_pieces; //player 2 pieces

};

#endif