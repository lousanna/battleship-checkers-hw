#include "battleship.h"
#include <cassert>
#include <fstream>
#include <string>

using namespace std;

void game1(void);
void game2(void);
bool compare_files(string f1,string f2);

int main (void){
   game1();
   game2();
   cout<<"SUCCESS!\n";
   return 0;
}

void game1(void){
    ofstream outFile;
    outFile.open("bs_out_1.txt");
    ifstream inFile;
    inFile.open("bs_in_1.txt");
    BattleshipGame bs = BattleshipGame(outFile,inFile);
    assert(bs.attack_square(make_pair(1,0))==RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(-10,11)) == RESULT_INVALID);//player 2 attacks,test range
    assert(bs.attack_square(make_pair(0,0)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(1,1)) == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(0,0)) == RESULT_KEEP_PLAYING);//player 2 attacks the same square

    assert(bs.attack_square(make_pair(1,2)) == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(1,0)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(1,3)) == RESULT_KEEP_PLAYING);//player 1 SUNK BATTLESHIP
    assert(bs.attack_square(make_pair(3,1)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(5,2)) == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(5,5)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(6,2)) == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(6,6)) == RESULT_KEEP_PLAYING);//player 2 MISS

    assert(bs.attack_square(make_pair(7,2)) == RESULT_KEEP_PLAYING);//player 1 SUNK CRUISER
    assert(bs.attack_square(make_pair(3,4)) == RESULT_KEEP_PLAYING);//player 2 MISS

    assert(bs.attack_square(make_pair(9,3)) == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(0,4)) == RESULT_KEEP_PLAYING);//player 2 MISS

    assert(bs.attack_square(make_pair(9,5)) == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(3,0)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(9,4)) == RESULT_KEEP_PLAYING);//player 1 SUNK SUBMARINE
    assert(bs.attack_square(make_pair(1,1)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(0,9)) == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(3,7)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(1,9)) == RESULT_KEEP_PLAYING);//player 1 SUNK DESTROYER
    assert(bs.attack_square(make_pair(1,4)) == RESULT_KEEP_PLAYING);//player 1 HIT

    assert(bs.attack_square(make_pair(3,5)) == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(7,7)) == RESULT_KEEP_PLAYING);//player 2 MISS

    assert(bs.attack_square(make_pair(4,5)) == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(1,3)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(5,5)) == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(6,5)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(6,5)) == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(4,0)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(7,5)) == RESULT_PLAYER1_WINS);//player 1 WON
    outFile.close();
    inFile.close();
    assert(compare_files("bs_out_1.txt","bs_out_2.txt"));
}

void game2(void){
    ofstream outFile;
    outFile.open("bs_out_3.txt");
    ifstream inFile;
    inFile.open("bs_in_2.txt");
    BattleshipGame bs = BattleshipGame(outFile,inFile);
    assert(bs.attack_square(make_pair(1,0))==RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(-10,11)) == RESULT_INVALID);//player 2 attacks,test range
    assert(bs.attack_square(make_pair(0,0)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(1,0)) == RESULT_KEEP_PLAYING);//player 1 attacks the same square
    assert(bs.attack_square(make_pair(1,0)) == RESULT_KEEP_PLAYING);//player 2 HIT 

    assert(bs.attack_square(make_pair(1,1)) == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(2,0)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(2,1)) == RESULT_KEEP_PLAYING);//player 1 MISS
    assert(bs.attack_square(make_pair(3,0)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(1,2)) == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(4,0)) == RESULT_KEEP_PLAYING);//player 2 SUNK AIRCRAFT CARRIER

    assert(bs.attack_square(make_pair(1,3)) == RESULT_KEEP_PLAYING);//player 1 SUNK BATTLESHIP
    assert(bs.attack_square(make_pair(1,1)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(1,2)) == RESULT_KEEP_PLAYING);//player 1 attacks a sunk ship
    assert(bs.attack_square(make_pair(1,2)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(0,0)) == RESULT_KEEP_PLAYING);//player 1 MISS
    assert(bs.attack_square(make_pair(1,3)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(5,6)) == RESULT_KEEP_PLAYING);//player 1 MISS
    assert(bs.attack_square(make_pair(1,4)) == RESULT_KEEP_PLAYING);//player 2 SUNK BATTLESHIP

    assert(bs.attack_square(make_pair(6,6)) == RESULT_KEEP_PLAYING);//player 1 MISS
    assert(bs.attack_square(make_pair(2,1)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(0,9)) == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square(make_pair(3,1)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(8,7)) == RESULT_KEEP_PLAYING);//player 1 MISS
    assert(bs.attack_square(make_pair(4,1)) == RESULT_KEEP_PLAYING);//player 2 SUNK CRUISER

    assert(bs.attack_square(make_pair(4,5)) == RESULT_KEEP_PLAYING);//player 1 MISS
    assert(bs.attack_square(make_pair(3,6)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(5,6)) == RESULT_KEEP_PLAYING);//player 1 MISS
    assert(bs.attack_square(make_pair(3,7)) == RESULT_KEEP_PLAYING);//player 2 SUNK DESTROYER

    assert(bs.attack_square(make_pair(5,5)) == RESULT_KEEP_PLAYING);//player 1 MISS
    assert(bs.attack_square(make_pair(5,5)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(6,5)) == RESULT_KEEP_PLAYING);//player 1 MISS
    assert(bs.attack_square(make_pair(6,5)) == RESULT_KEEP_PLAYING);//player 2 HIT

    assert(bs.attack_square(make_pair(7,5)) == RESULT_KEEP_PLAYING);//player 1 MISS
    assert(bs.attack_square(make_pair(7,5)) == RESULT_PLAYER2_WINS);//player 2 WON
    outFile.close();
    inFile.close();
    assert(compare_files("bs_out_3.txt","bs_out_4.txt"));
}


bool compare_files(string f1, string f2)
{
    ifstream is1(f1), is2(f2);
    if(!is1.is_open() || !is2.is_open()) {
        cerr << "could not open one of the files" << std::endl;
        return false;
    }
    char c1, c2;
    while(is1.get(c1)) {
        is2.get(c2);
        if(c1 != c2) {
            return false;
        }
    }
    return is1.eof() != is2.eof();
}

