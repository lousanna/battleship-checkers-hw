#include "mobile_battleship.h"
#include <cassert>
#include <fstream>
#include <string>

using namespace std;

void game1(void);
bool compare_files(string f1,string f2);

int main (void){
    game1();
    cout<<"SUCCESS!\n";
    return 0;
}

void game1(void){
    ofstream outFile;
    outFile.open("bs_out_5.txt");
    ifstream inFile;
    inFile.open("bs_in_1.txt");
    MobileBSGame bs = MobileBSGame(outFile,inFile);
    assert(bs.attack_square("10")==RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square("-1011") == RESULT_INVALID);//player 2 attacks,test range
    assert(bs.attack_square("00") == RESULT_KEEP_PLAYING);//player 2 HIT
    
    assert(bs.attack_square("10r5") == RESULT_INVALID);//INVALID movement
    assert(bs.attack_square("10r3") == RESULT_KEEP_PLAYING);//Valid movement Player 1 Right 3
    
    assert(bs.attack_square("10l1") == RESULT_INVALID);//INVALID movement
    assert(bs.attack_square("10d1") == RESULT_KEEP_PLAYING);//Valid movement Player 2 Down 1
    
    assert(bs.attack_square("11d2") == RESULT_KEEP_PLAYING);//Valid movement Player 1 Down 2
    assert(bs.attack_square("11u1") == RESULT_KEEP_PLAYING);//Valid movement Player 2 Up 1
    
    assert(bs.attack_square("02") == RESULT_KEEP_PLAYING);//player 1 MISS
    assert(bs.attack_square("30") == RESULT_KEEP_PLAYING);//player 2 attacks the same square
    
    assert(bs.attack_square("10a5") == RESULT_INVALID);//INVALID movement
    assert(bs.attack_square("-1r5") == RESULT_INVALID);//INVALID movement
    assert(bs.attack_square("11dd") == RESULT_INVALID);//INVALID movement
    assert(bs.attack_square("11D1") == RESULT_INVALID);//INVALID movement
    
    assert(bs.attack_square("13") == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square("02") == RESULT_KEEP_PLAYING);//player 2 MISS
    
    assert(bs.attack_square("11") == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square("40") == RESULT_KEEP_PLAYING);//player 2 HIT
    
    assert(bs.attack_square("52") == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square("55") == RESULT_KEEP_PLAYING);//player 2 HIT
    
    assert(bs.attack_square("62") == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square("66") == RESULT_KEEP_PLAYING);//player 2 MISS
    
    assert(bs.attack_square("72") == RESULT_KEEP_PLAYING);//player 1 SUNK CRUISER
    assert(bs.attack_square("34") == RESULT_KEEP_PLAYING);//player 2 MISS
    
    assert(bs.attack_square("93") == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square("04") == RESULT_KEEP_PLAYING);//player 2 MISS
    
    assert(bs.attack_square("95") == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square("30") == RESULT_KEEP_PLAYING);//player 2 HIT
    
    assert(bs.attack_square("94") == RESULT_KEEP_PLAYING);//player 1 SUNK SUBMARINE
    assert(bs.attack_square("13") == RESULT_KEEP_PLAYING);//player 2 HIT
    
    assert(bs.attack_square("09") == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square("37") == RESULT_KEEP_PLAYING);//player 2 HIT
    
    assert(bs.attack_square("19") == RESULT_KEEP_PLAYING);//player 1 SUNK DESTROYER
    assert(bs.attack_square("14") == RESULT_KEEP_PLAYING);//player 1 HIT
    
    assert(bs.attack_square("35") == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square("77") == RESULT_KEEP_PLAYING);//player 2 MISS
    
    assert(bs.attack_square("45") == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square("15") == RESULT_KEEP_PLAYING);//player 2 HIT
    
    assert(bs.attack_square("55") == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square("65") == RESULT_KEEP_PLAYING);//player 2 HIT
    
    assert(bs.attack_square("65") == RESULT_KEEP_PLAYING);//player 1 HIT
    assert(bs.attack_square("40") == RESULT_KEEP_PLAYING);//player 2 HIT
    
    assert(bs.attack_square("75") == RESULT_KEEP_PLAYING);//SUNK AIRCRAFT CARRIER
    assert(bs.attack_square("16") == RESULT_KEEP_PLAYING);//SUNK BATTLESHIP
    assert(bs.attack_square("12") == RESULT_PLAYER1_WINS);//SUNK BATTLESHIP

    outFile.close();
    inFile.close();
    assert(compare_files("bs_out_5.txt","bs_out_6.txt"));
}

bool compare_files(string f1, string f2)
{
    ifstream is1(f1), is2(f2);
    if(!is1.is_open() || !is2.is_open()) {
        cerr << "could not open one of the files" << std::endl;
        return false;
    }
    char c1, c2;
    while(is1.get(c1)) {
        is2.get(c2);
        if(c1 != c2) {
            return false;
        }
    }
    return is1.eof() != is2.eof();
}

