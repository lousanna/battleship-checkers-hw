#include <string>
#include <cctype>

#include "checkers.h"
#include "mobile_battleship.h"

void gameBattleship(void);
void gameMobileBattleship(void);
void gameCheckers(void);

using namespace std;

int main (void){
    cout<<"CHOOSE A GAME:";
    char choice;
    cin>>choice;
    string temp;
    try{
	 getline(cin,temp);//get rid of the newline character
	 switch (choice){
	 case '1':gameBattleship();
		break;
	 case '2':gameMobileBattleship();
	    	break;
	 case '3':gameCheckers();
		break;
	 default:
		cout<<"Please enter 1,2 or 3"<<endl;
	 }
    }
    catch  (const char *s){
	return 0;
    }
    return 0;
}

void gameBattleship(void){
    BattleshipGame bs = BattleshipGame();
    GameResult result = RESULT_KEEP_PLAYING;
    string input;
    int x = 0;
    int y = 0;
    while (result <RESULT_PLAYER1_WINS){
	bool flag = false;//false means input is in the wrong format
	//get the correct input
	while (!flag){
	    cout<<bs;
	    cin>>input;
	    if (input == "q"){
		throw "quit";
	    }
	    if (input.length()==2 && isdigit(input[0]) &&isdigit(input[1])){
		flag = true;
	    }
	    else{
		cout<<"INVALID MOVE\n";
	    }
	}
	x = input[0]-'0';
	y = input[1]-'0';
	result = bs.attack_square(make_pair(x,y));
	if (result == RESULT_INVALID){
	    cout<<"INVALID MOVE\n";
	}
    }
    if (result == RESULT_PLAYER1_WINS){
	cout<<"PLAYER 1 WON\n";
    }
    else {
	if (result == RESULT_PLAYER2_WINS){
	    cout<<"PLAYER 2 WON\n";
	}
    }
}

void gameMobileBattleship(void){

    MobileBSGame mbs = MobileBSGame();
    GameResult result = RESULT_KEEP_PLAYING;
    
    while (result < RESULT_PLAYER1_WINS){
        string input;

        bool valid = false;
        do {
            cout<<mbs;
            cin>>input;
            if (input == "q"){
                throw "quit";
            }

            valid = mbs.validMove(input);
            if (!valid) {
                cout<<"INVALID MOVE\n";
            }
        } while (!valid);

        result = mbs.attack_square(input);

    }
    if (result == RESULT_PLAYER1_WINS){
        cout<<"PLAYER 1 WON\n";
    }
    else if (result == RESULT_PLAYER2_WINS){
        cout<<"PLAYER 2 WON\n";
    }
}

void gameCheckers(void){

    CheckersGame checker_board = CheckersGame();
    GameResult result = RESULT_KEEP_PLAYING;
    int move_result= 0;
    string input;
    
    while(result == RESULT_KEEP_PLAYING){
    	cout<<checker_board;
    	cin>>input;
    	if (input== "q"){
    		throw "quit";
    	}
    	move_result=checker_board.move_piece(input);
	    checker_board.set_game_status();
    	result = checker_board.return_game_status();
	    if (result != RESULT_KEEP_PLAYING){
		     cout<<checker_board;
             return;
        }

    	if(move_result==-1){
    		/*cout << "Didn't select your own piece"<<endl;*/
            cout << "INVALID MOVE\n";
    	}else if(move_result==-2){
            cout << "INVALID MOVE\n";
    		/*cout << "You must jump a piece this turn" <<endl;*/
    	}else if(move_result==-3){
    		cout << "INVALID MOVE\n";
    	}else if(move_result==-4){
		     cout<< "JUMPED\n";
	    }else if(move_result ==-5){
		     cout<< "CROWNED\n";
    	}else if(move_result==0){
    	}

    	

    }
    
    


}
