 #ifndef GAME_H
#define GAME_H

/*
 * Use this file as a *starting point*.  You may add more classes and other
 * definitions here.
 *
 * We have suggested a couple ideas (Board and Game) for base classes you can
 * use in your design.
 *
 * Implement derived types for the games in "battleship.h" and "tictactoe.h"
 *
 * You must use "enum GameResult" and "Coord" as defined here, and you must
 * implement derived types called BattleshipGame and TicTacToeGame, each with
 * a public attack_square member function, as you can see from reading
 * play_bs.cpp and play_ttt.cpp.
 */

#include <utility>
#include <cassert>
#include <iostream>
enum GameResult {
	RESULT_KEEP_PLAYING, // turn was valid and game is not over yet
	RESULT_INVALID,      // turn was invalid; e.g. attacked square
	                     // was attacked previously
	RESULT_STALEMATE,
	RESULT_PLAYER1_WINS, // game over, player 1 wins
	RESULT_PLAYER2_WINS  // game over, player 2 wins
};

typedef std::pair<int, int> Coord;

class Board {
public:
// a constructor that creates the  board 
// and initialize all the values to 0
Board(): ary(nullptr), size(0) { }


Board(int sz) :  size(sz) {
	ary= new int*[sz];
	for(int i = 0; i < sz; i++){
    	ary[i] = new int[sz];
    	}
	for (int i=0;i<sz;i++){
		for (int j=0;j<size;j++)
		ary[i][j] = 0;
	}
}

//the destructor
~Board(){
	for (int i = 0; i < size; i++){
  		delete [] ary[i];
  	}
	delete [] ary;
}

//set the value of a grid

void set_grid(Coord coordinate, int value){
	int x=coordinate.first;
	int y= coordinate.second;
	assert(x < size && y < size && x>=0 && y>=0);
	ary[x][y]=value;
}

int get_value(Coord coordinate){
	int x=coordinate.first;
    	int y= coordinate.second;
	assert(x < size && y < size && x>=0 && y>=0);
	return ary[x][y];

}

friend std::ostream& operator<<(std::ostream& out, const Board& board){
	for (int i=0;i<board.size;i++){
	    for (int j=0;j<board.size;j++){
		out<<board.ary[j][i]<<" ";
	    }
	    out<<std::endl;
	}
	return out;
}
private:
//a 2D array of integers
	int **ary;
	int size;
	
};

class Game {
public:

Game(): counter(1),game_over(0){}


int return_player () const{
	if (counter%2==0){
		return 2;
	}
	else{
		return 1;
	}
}

//ostream& operator<<()
//prints out player prompt or who wins
friend std::ostream& operator<<(std::ostream& out,const Game & game){
	if (game.game_over){
	    if(game.game_over==3){
		//out <<"No moves available\n";
	    }
	    else{
		out<<"PLAYER "<<game.game_over<<" WON\n";
	    }
	}
	else{
	    out<<"PLAYER "<<game.return_player()<<":";
	}
	return out;
}

protected:
	int counter;
	int game_over;//0 for not over, 1 for player1 wins, 2 for player 2 wins, 3 for no more moves (checkers)
};

#endif
